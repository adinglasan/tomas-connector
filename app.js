'use strict'

const Reekoh = require('reekoh')
const plugin = new Reekoh.plugins.Connector()
const rkhLogger = new Reekoh.logger('tomas-connector')
const request = require('request-promise')
const get = require('lodash.get')
const xml2js = require('xml2js')
const builder = new xml2js.Builder()

let payloadTemplate = {
  's:Envelope': {
    '$': {
      'xmlns:s': 'http://schemas.xmlsoap.org/soap/envelope/'
    },
    's:Header': [
      ''
    ],
    's:Body': [
      {
        '$': {
          'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
          'xmlns:xsd': 'http://www.w3.org/2001/XMLSchema'
        },
        'DefectInstance_DoCreate': [
          {
            '$': {
              'xmlns': 'http://TechnologyOneCorp.com/T1.W1.Public/Services'
            },
            'Request': [
              {
                '$': {
                  'PerformSaveAttachments': 'false'
                },
                'Auth': [
                  {
                    '$': {
                      'UserId': 'REEKOHWSUSER',
                      'Password': 'kl34!56Vs2',
                      'Config': 'MBRC-TEST-CES direct',
                      'FunctionName': '$W1.DEFINST.CREAT.WS'
                    }
                  }
                ],
                'DefectInstances': [
                  {
                    'DefectInstances': [
                      {
                        '$': {
                          'DefectDate': '2018-07-01T11:23:54',
                          'Source': 'IM',
                          'Status': 'I',
                          'State': 'Added',
                          'DefectCode': 'SW035',
                          'AssetNumber': 'A00042083',
                          'RegisterName': 'MBRCOP',
                          'Stage': 'R'
                        },
                        'Attachments': [
                          ''
                        ],
                        'Geographies': [
                          {
                            '$': {
                              'ElementType': '1',
                              'State': 'Added'
                            },
                            'Points': [
                              {
                                '$': {
                                  'Latitude': '-27.1717240000000011',
                                  'Longitude': '152.98217099999999391'
                                }
                              }
                            ]
                          }
                        ],
                        'DefectCodeCustomFieldValues': [
                          {
                            'DefectCodeCustomFieldValues': [
                              {
                                '$': {
                                  'ValAlpha': '3',
                                  'ValNum': '0',
                                  'ValDate': '2018-07-01T11:23:54',
                                  'ValDateTime': '2018-07-01T11:23:54'
                                }
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
}

plugin.on('data', (data) => {
  // console.log(payloadTemplate['s:Envelope']['s:Body'].DefectInstance_DoCreate.Request.DefectInstances.DefectInstances.DefectCodeCustomFieldValues.DefectCodeCustomFieldValues)
  // console.log(payloadTemplate['s:Envelope']['s:Body'][0].DefectInstance_DoCreate[0].Request[0].DefectInstances[0].DefectInstances[0].DefectCodeCustomFieldValues[0].DefectCodeCustomFieldValues)
  payloadTemplate['s:Envelope']['s:Body'][0].DefectInstance_DoCreate[0].Request[0].DefectInstances[0].DefectInstances[0].DefectCodeCustomFieldValues[0].DefectCodeCustomFieldValues.push({
    'DefectCodeCustomFieldValue 1': get(data, 'pothole_id'),
    'DefectCodeCustomFieldValue 2': get(data, 'severity'),
    'DefectCodeCustomFieldValue 3': get(data, 'type'),
    'DefectCodeCustomFieldValue 4': get(data, 'last_seen'),
    'DefectCodeCustomFieldValue 5': get(data, 'passed'),
    'DefectCodeCustomFieldValue 6': get(data, 'missed'),
    'DefectCodeCustomFieldValue 7': get(data, 'detected')
  })

  console.log(builder.buildObject(payloadTemplate))
})

plugin.once('ready', () => {
  plugin.log('Tomas Connector has been initialized.')
  rkhLogger.info('Tomas Connector has been initialized.')
  plugin.emit('init')
})

module.exports = plugin
