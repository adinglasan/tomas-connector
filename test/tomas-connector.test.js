/* global describe, it, after, before */
'use strict'

const amqp = require('amqplib')

let _channel = null
let _conn = null
let app = null

let _app = null

describe('Tomas Connector Test', () => {
  before('init', function () {
    process.env.LOGGERS = 'logger1,logger2'
    process.env.EXCEPTION_LOGGERS = 'exlogger1,exlogger2'
    process.env.BROKER = 'amqp://guest:guest@127.0.0.1/'
    process.env.INPUT_PIPE = 'demo.pipe.connector'
    process.env.ACCOUNT = 'demo account'

    amqp.connect(process.env.BROKER)
      .then((conn) => {
        _conn = conn
        return conn.createChannel()
      }).then((channel) => {
        _channel = channel
      }).catch((err) => {
        console.log(err)
      })
  })

  after('close connection', function (done) {
    _conn.close()
    done()
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(10000)
      app = require('../app')
      app.once('init', done)
    })
  })

  describe('#data', function () {
    it('should send data to third party client', (done) => {
      this.timeout(15000)

      let data = {
        'pothole_id': '3a11482a-3bcb-11e9-b210-d663bd873d93',
        'latitude': '-27.16950200000000137',
        'longitude': '152.98694100000000162',
        'classification': 'pothole',
        'severity': '0',
        'type': 'repaired',
        'detected': '20180701112354',
        'last_seen': '20190301081352',
        'passed': '12',
        'missed': '0',
        'AssetNumber': 'A00550473'
      }

      _channel.sendToQueue('demo.pipe.connector', Buffer.from(JSON.stringify(data)))
      setTimeout(done, 10000)
    })
  })
})
